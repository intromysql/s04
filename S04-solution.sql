-- Create an solution.sql file inside s04/a1 project and do the following using the music_db database:

-- Find all artists that has letter d in its name.
select * from artists where artists.name like 'd%';

-- Find all songs that has a length of less than 230.
select * from songs where songs.length< '00:02:30';

-- Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
select albums.album_title,songs.song_name, songs.length from albums INNER JOIN songs on albums.id=songs.id;


-- Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
select albums.album_title from albums inner join artists on albums.id=artists.id where albums.album_title like 'a%' ;

-- Sort the albums in Z-A order. (Show only the first 4 records.)
select albums.id,albums.album_title from albums where albums.id<5 ORDER by albums.id DESC;

-- Join the 'albums' and 'songs' tables. (Sort albums from Z-A)
select albums.id,albums.album_title,songs.song_name from albums INNER JOIN songs on albums.id=songs.id  ORDER by albums.id DESC;

